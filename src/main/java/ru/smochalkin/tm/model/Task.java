package ru.smochalkin.tm.model;

public final class Task extends AbstractBusinessEntity {

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
