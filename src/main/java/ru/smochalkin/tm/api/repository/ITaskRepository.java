package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String taskId);

    List<Task> findTasksByProjectId(String projectId);

    void removeTasksByProjectId(String projectId);

}
