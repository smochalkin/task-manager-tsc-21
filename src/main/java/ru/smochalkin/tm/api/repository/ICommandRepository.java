package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractSystemCommand> getArguments();

    List<String> getCommandNames();

    List<String> getCommandArgs();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

}
