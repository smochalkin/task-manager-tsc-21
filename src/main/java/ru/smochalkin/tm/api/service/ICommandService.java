package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.model.Command;

import java.util.Collection;
import java.util.List;

public interface ICommandService {


    Collection<AbstractCommand> getCommands();

    Collection<AbstractSystemCommand> getArguments();

    List<String> getCommandNames();

    List<String> getCommandArgs();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);
}
