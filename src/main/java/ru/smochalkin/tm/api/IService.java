package ru.smochalkin.tm.api;

import ru.smochalkin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IService <E extends AbstractEntity> extends IRepository<E> {
}
