package ru.smochalkin.tm.api.model;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

}
