package ru.smochalkin.tm.api.model;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    void setStartDate(Date name);

}
