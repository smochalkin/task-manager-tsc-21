package ru.smochalkin.tm.comparator;

import ru.smochalkin.tm.api.model.IHasCreated;
import java.util.Comparator;

public class ComparatorByCreated implements Comparator<IHasCreated> {

    public final static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {
    }

    @Override
    public int compare(IHasCreated o1, IHasCreated o2) {
        if (o1.getCreated() == null) return 1;
        if (o2.getCreated() == null) return -1;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
