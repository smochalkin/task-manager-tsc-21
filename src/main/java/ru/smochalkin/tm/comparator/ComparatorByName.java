package ru.smochalkin.tm.comparator;

import ru.smochalkin.tm.api.model.IHasName;
import java.util.Comparator;

public class ComparatorByName implements Comparator<IHasName> {

    public final static ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName(){}

    @Override
    public int compare(IHasName o1, IHasName o2) {
        if (o1.getName() == null) return 1;
        if (o2.getName() == null) return -1;
        return o1.getName().compareTo(o2.getName());
    }

}