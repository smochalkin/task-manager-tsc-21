package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.List;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Display list of arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        List<String> keys = serviceLocator.getCommandService().getCommandArgs();
        for (String value : keys) {
            if (value == null || value.isEmpty()) continue;
            System.out.println(value);
        }
    }

}
