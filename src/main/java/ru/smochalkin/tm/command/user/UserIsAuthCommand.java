package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;

public class UserIsAuthCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-auth";
    }

    @Override
    public String description() {
        return "Check user auth.";
    }

    @Override
    public void execute() {
        System.out.println("User auth: " + serviceLocator.getAuthService().isAuth());
    }

}
