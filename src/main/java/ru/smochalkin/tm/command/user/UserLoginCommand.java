package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Log in.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
