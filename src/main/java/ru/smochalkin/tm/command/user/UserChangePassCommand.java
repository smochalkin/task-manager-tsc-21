package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserChangePassCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-change-password";
    }

    @Override
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter new password:");
        String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}

