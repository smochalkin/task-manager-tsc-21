package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "Start task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getTaskService().findByIndex(userId, index);
        serviceLocator.getTaskService().updateStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
