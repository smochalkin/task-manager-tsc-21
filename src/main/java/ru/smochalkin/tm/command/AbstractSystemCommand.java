package ru.smochalkin.tm.command;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public abstract String arg();

    @Override
    public String toString() {
        String result = super.toString();
        String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
