package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        return taskRepository.bindTaskById(projectId, taskId);
    }

    @Override
    public Task unbindTaskByProjectId(final String projectId, final String taskId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        projectRepository.findById(projectId);
        taskRepository.findById(taskId);
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findTasksByProjectId(final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeTasksByProjectId(id);
        projectRepository.removeById(id);
    }

    @Override
    public void removeProjectByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        Project project = projectRepository.findByName(userId, name);
        removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex(final String userId, final Integer index) {
        Project project = projectRepository.findByIndex(userId, index);
        removeProjectById(project.getId());
    }

    @Override
    public boolean isProjectId(final String id) {
        return projectRepository.findById(id) != null;
    }

    @Override
    public boolean isProjectName(final String userId, final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findByName(userId, name) != null;
    }

    @Override
    public boolean isProjectIndex(final String userId, final Integer index) {
        if (index == null || index < 0) return false;
        if (index >= projectRepository.getCountByUser(userId)) return false;
        return true;
    }

    @Override
    public boolean isTaskId(final String id) {
        return taskRepository.findById(id) != null;
    }

}
