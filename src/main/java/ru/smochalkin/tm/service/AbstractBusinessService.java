package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    protected final IBusinessRepository<E> businessRepository;

    public AbstractBusinessService(final IBusinessRepository<E> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    @Override
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        businessRepository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        return businessRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (isEmpty(userId)) throw new EmptyIdException();
        return businessRepository.findAll(userId, comparator);
    }

    @Override
    public E findByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findByName(userId, name);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (!isIndex(userId, index)) throw new IndexIncorrectException();
        return businessRepository.findByIndex(userId, index);
    }

    @Override
    public E removeByName(final String userId, final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeByName(userId, name);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (!isIndex(userId, index)) throw new IndexIncorrectException();
        return businessRepository.removeByIndex(userId, index);
    }

    @Override
    public E updateById(final String id, final String name, final String desc) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateById(id, name, desc);
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String desc) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (!isIndex(userId, index)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateByIndex(userId, index, name, desc);
    }

    @Override
    public E updateStatusById(final String id, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        return businessRepository.updateStatusById(id, status);
    }

    @Override
    public E updateStatusByName(final String userId, final String name, final Status status) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateStatusByName(userId, name, status);
    }

    @Override
    public E updateStatusByIndex(final String userId, final Integer index, final Status status) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (!isIndex(userId, index)) throw new IndexIncorrectException();
        return businessRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public boolean isIndex(final String userId, final Integer index) {
        if (index == null || index < 0) return false;
        if (index >= businessRepository.getCountByUser(userId)) return false;
        return true;
    }

}
