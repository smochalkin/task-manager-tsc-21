package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.List;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractSystemCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public List<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public List<String> getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public void add(final AbstractCommand command) {
        commandRepository.add(command);
    }

}
