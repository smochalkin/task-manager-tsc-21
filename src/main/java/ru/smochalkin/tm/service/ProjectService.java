package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.model.Project;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        businessRepository.add(project);
        return project;
    }

}
